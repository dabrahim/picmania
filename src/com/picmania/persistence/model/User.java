package com.picmania.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@Table(name="user")
@NamedQueries({
	@NamedQuery(name="User.findAll", query="SELECT u FROM User u"),
	@NamedQuery(name="User.hasValidCredentials", query="SELECT u FROM User u WHERE u.login = :login AND u.password = :password"),
	@NamedQuery(name="User.findByLogin", query="SELECT u FROM User u WHERE u.login = :login")
})
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name="avatar_url")
	private String avatarUrl;

	@Column(name="first_name")
	private String firstName;

	@Column(name="last_name")
	private String lastName;

	private String login;

	private String password;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="registration_date")
	private Date registrationDate;

	@Enumerated(EnumType.STRING)
	private Type type;
	
	public enum Type {
		ADMIN,
		BASIC,
		UNKNOWN
	}

	//bi-directional many-to-one association to Album
	@OneToMany(mappedBy="user", fetch = FetchType.LAZY)
	private List<Album> albums;

	//bi-directional many-to-many association to Album
	@ManyToMany
	@JoinTable(
		name="album_access"
		, joinColumns={
			@JoinColumn(name="user_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="album_id")
			}
		)
	private List<Album> allowedAlbums;

	public User() {
	}

	public User(String firstName, String lastName, String login, String password, Type type) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.login = login;
		this.password = password;
		this.type = type;
	}
	
	public User(String firstName, String lastName, String login, String password, Type type, String avatarUrl) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.login = login;
		this.password = password;
		this.type = type;
		this.avatarUrl = avatarUrl;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAvatarUrl() {
		return this.avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getRegistrationDate() {
		return this.registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Type getType() {
		return this.type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public List<Album> getAlbums() {
		return this.albums;
	}

	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}

	public Album addAlbum(Album album) {
		getAlbums().add(album);
		album.setUser(this);

		return album;
	}

	public Album removeAlbum(Album album) {
		getAlbums().remove(album);
		album.setUser(null);

		return album;
	}

	public List<Album> getAllowedAlbums() {
		return this.allowedAlbums;
	}

	public void setAllowedAlbums(List<Album> allowedAlbums) {
		this.allowedAlbums = allowedAlbums;
	}
	
	@PrePersist
	public void init() {
		this.registrationDate = new Date();
		
		if(this.type == Type.UNKNOWN) {
			this.type = Type.BASIC;
		}
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", avatarUrl=" + avatarUrl + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", login=" + login + ", password=" + password + ", type=" + type + "]";
	}

}