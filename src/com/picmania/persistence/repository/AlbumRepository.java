package com.picmania.persistence.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import com.picmania.persistence.dao.AlbumDAO;
import com.picmania.persistence.model.Album;

@Stateless
public class AlbumRepository extends AbstractRepository implements AlbumDAO {

	@Override
	public Long create(Album album) {
		em.persist(album);
		em.flush();
		return album.getId();
	}

	@Override
	public void delete(Long id) {
		Album album = em.find(Album.class, id);
		em.remove(album);
	}

	@Override
	public List<Album> findAll() {
		TypedQuery<Album> query = em.createNamedQuery("Album.findAll", Album.class);
		return query.getResultList();
	}

	@Override
	public Album findById(Long id) {
		return em.find(Album.class, id);
	}

	@Override
	public Album update(Album album) {
		em.merge(album);
		return album;
	}

}
