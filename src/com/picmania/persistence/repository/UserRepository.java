package com.picmania.persistence.repository;


import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.picmania.persistence.dao.UserDAO;
import com.picmania.persistence.model.User;

@Stateless
public class UserRepository extends AbstractRepository implements UserDAO {
	
	public Long create(User user) {
		em.persist(user);
		em.flush();
		return user.getId();
	}

	public void delete(Long id) {
		User user = em.find(User.class, id);
		em.remove(user);
	}

	public List<User> findAll() {
		TypedQuery<User> query = em.createNamedQuery("User.findAll", User.class);
		return query.getResultList();
	}

	public User findById(Long id) {
		return em.find(User.class, id);
	}

	public User update(User user) {
		em.merge(user);
		return user;
	}

	@Override
	public User findByLogin(String login) {
		TypedQuery<User> query = em.createNamedQuery("User.findByLogin", User.class);
		query.setParameter("login", login);
		return query.getSingleResult();
	}

	@Override
	public boolean hasValidCredentials(String login, String password) {
		TypedQuery<User> query = em.createNamedQuery("User.hasValidCredentials", User.class);
		query.setParameter("login", login);
		query.setParameter("password", password);
		
		try {
			query.getSingleResult();
			
		} catch(NoResultException e) {
			return false;
		}
		
		return true;
	}
}

