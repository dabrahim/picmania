package com.picmania.persistence.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractRepository {
	@PersistenceContext(unitName = "photomaniaPU")
	protected EntityManager em;
}
