package com.picmania.persistence.dao;

import javax.ejb.Local;

import com.picmania.persistence.model.User;

@Local
public interface UserDAO extends BaseDAO<User> {
	
	User findByLogin(String login);	
	
	boolean hasValidCredentials(String login, String password);
}
