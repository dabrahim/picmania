package com.picmania.persistence.dao;

import javax.ejb.Local;

import com.picmania.persistence.model.Album;

@Local
public interface AlbumDAO extends BaseDAO<Album> {

}
