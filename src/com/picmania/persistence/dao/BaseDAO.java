package com.picmania.persistence.dao;

import java.util.List;

public interface BaseDAO <T> {
	Long create(T t);

	void delete(Long id);

	List<T> findAll();

	T findById(Long id);

	T update(T t);
}
