package com.picmania;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.io.FilenameUtils;

public class FileUploadManager {
	private static final String UPLOAD_DIR_PATH_PARAM_NAME = "upload.location";
	private static final int FILE_NAME_LENGTH = 50;
	private String uploadDirectoryPath;
	private HttpServletRequest request;
	private String lastFileName;
	private String errorMessage;
	
	public FileUploadManager(ServletContext servletContext, HttpServletRequest request) {
		this.uploadDirectoryPath = servletContext.getInitParameter(UPLOAD_DIR_PATH_PARAM_NAME);
		this.request = request;
	}
	
	public boolean save(String parameterName) {
		boolean success = false;
		
		try {
			Part filePart = this.request.getPart(parameterName);
			String fileExtension = FilenameUtils.getExtension(filePart.getSubmittedFileName());
			String filename = Utils.generateRandomFileName(FILE_NAME_LENGTH, fileExtension);
			
			File file = new File(this.uploadDirectoryPath, filename);
			
			try (InputStream input = filePart.getInputStream()) {
			    Files.copy(input, file.toPath());
			    setLastFileName(filename);
			    success = true;
			}

		} catch(Exception e) {
			setErrorMessage(e.getMessage());
		}
		
		return success;
	}

	public String getLastUploadedFileName() {
		return lastFileName;
	}
	
	public String getLastUploadedFileContextualPath() {
		return String.format("/uploads/%s", getLastUploadedFileName());
	}

	private void setLastFileName(String lastFileName) {
		this.lastFileName = lastFileName;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	private void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
