package com.picmania.controller.user;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import com.picmania.TokenManager;
import com.picmania.Utils;
import com.picmania.controller.core.BaseController;
import com.picmania.persistence.dao.UserDAO;
import com.picmania.persistence.model.User;

public class UserLoginController extends BaseController<UserDAO> {

	@Override
	public void onRouteTriggered() throws IOException, ServletException {
		String login = getParam("login");
		String password = getParam("password");
		String hashedPassword = Utils.hash(password);
		
		if(getDao().hasValidCredentials(login, hashedPassword)) {
			Map<String, Object> payload = new HashMap<String, Object>();
		
			User user = getDao().findByLogin(login);
			String jwt = TokenManager.getJwtToken(user.getId());
			
			payload.put("jwt", jwt);
			payload.put("user", user);
			
			sendResponse("Login successful", payload);
			
		} else {
			sendErrorResponse("Login or password incorrect");
		}
	}

}
