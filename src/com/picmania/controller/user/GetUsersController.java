package com.picmania.controller.user;

import java.io.IOException;
import java.util.List;

import com.picmania.controller.core.BaseController;
import com.picmania.persistence.dao.UserDAO;
import com.picmania.persistence.model.User;

public class GetUsersController extends BaseController<UserDAO> {

	@Override
	public void onRouteTriggered() throws IOException {
		List<User> userList = getDao().findAll();
		sendResponse(userList);
	}
}
