package com.picmania.controller.user;

import java.io.IOException;

import javax.servlet.ServletException;

import com.picmania.Utils;
import com.picmania.controller.core.BaseController;
import com.picmania.controller.core.RequestFile;
import com.picmania.persistence.dao.UserDAO;
import com.picmania.persistence.model.User;

public class CreateUserController extends BaseController<UserDAO> {
	
	@Override
	public void onRouteTriggered() throws IOException, ServletException {
		String firstName = getParam("firstName");
		String lastName = getParam("lastName");
		String login = getParam("login");
		String password = getParam("password");
		String avatarPath = null;
		
		// We check whether this login is free or not
		if(getDao().findByLogin(login) == null) {
			
			// Processing the sent avatar image
			if(hasRequestFile("avatar")) {
				RequestFile file = getRequestFile("avatar");
				
				if(file.save()) {
					avatarPath = file.getPublicPath();
				}
			}
			
			User user = new User(firstName, lastName, login, Utils.hash(password), User.Type.BASIC, avatarPath);
			
			getDao().create(user);
			
			sendResponse("Registration successful", user);
			
		} else {
			sendErrorResponse("This login already exists");
		}
	}
}
