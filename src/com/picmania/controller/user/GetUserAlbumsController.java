package com.picmania.controller.user;

import java.io.IOException;

import com.picmania.controller.core.BaseController;
import com.picmania.persistence.dao.UserDAO;
import com.picmania.persistence.model.User;

public class GetUserAlbumsController extends BaseController<UserDAO> {

	@Override
	public void onRouteTriggered() throws IOException{
		Long id = Long.parseLong(getParam(":id"));
		User user = getDao().findById(id);
		sendResponse(user.getAlbums());
	}

}
