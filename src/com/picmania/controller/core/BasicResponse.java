package com.picmania.controller.core;

public class BasicResponse<T> {
	private boolean success;
	private String message;
	private T payload;
	
	public BasicResponse() {
		super();
	}
	public BasicResponse(boolean succes, String message) {
		super();
		this.success = succes;
		this.message = message;
	}
	
	
	public BasicResponse(boolean success, String message, T payload) {
		super();
		this.success = success;
		this.message = message;
		this.payload = payload;
	}
	public boolean isSucces() {
		return success;
	}
	public void setSuccess(boolean succes) {
		this.success = succes;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getPayload() {
		return payload;
	}
	public void setPayload(T payload) {
		this.payload = payload;
	}
}
