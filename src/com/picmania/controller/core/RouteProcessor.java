package com.picmania.controller.core;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Ibrahima Ndiaye
 *
 */
public class RouteProcessor {
	private static final String PLACEHOLDER_PARAM_REGEX = "\\{[is]:([a-zA-Z0-9]+)}";
    private static final String PARAMETER_PATTERN = ".*\\{[is]:[a-zA-Z0-9]+}.*";

    public static Map<String, String> extractParams(String urlDefinition, String requestedUrl) {
        Map<String, String> paramMap = null;
        String builtRegex = buildRegex(urlDefinition);

        //We check whether the url matches the produced REGEX expression
        Pattern p = Pattern.compile(builtRegex);
        Matcher m = p.matcher(requestedUrl);

        if (m.find()) {
            Pattern pattern = Pattern.compile(PLACEHOLDER_PARAM_REGEX);
            Matcher matcher = pattern.matcher(urlDefinition);

            int index = 1;
            paramMap = new HashMap<>();

            while (matcher.find()) {
                String paramName = matcher.group(1);
                paramMap.put(paramName, m.group(index));
                index++;
            }
        }

        return paramMap;
    }

    public static boolean hasPlaceholderParam(String urlDefinition) {
        return Pattern.matches(PARAMETER_PATTERN, urlDefinition);
    }

    public static String buildRegex(String urlDefinition) {
        String builtUrl = urlDefinition.replaceAll("\\{i:[a-zA-Z]+}", "([0-9]+)")
                .replaceAll("\\{s:[a-zA-Z]+}", "([a-zA-Z]+)");
        return String.format("^%s$", builtUrl);
    }

    public static boolean matches(String urlDefinition, String requestedUrl) {
        String builtRegex = buildRegex(urlDefinition);
        return Pattern.matches(builtRegex, requestedUrl);
    }
    
    public static String normalize(String url) {
        if(url == null) return "/";

        if(!url.endsWith("/")) {
            url = String.format("%s/", url);
        }

        if (!url.startsWith("/")){
            url = String.format("/%s", url);
        }

        return url;
     }
}
