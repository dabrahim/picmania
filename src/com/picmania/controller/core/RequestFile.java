package com.picmania.controller.core;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import javax.servlet.http.Part;

import org.apache.commons.io.FilenameUtils;

import com.picmania.Utils;

public class RequestFile {
	private static final int FILE_NAME_LENGTH = 50;
	private Part filePart;
	private String uploadDir;
	private String errorMessage;
	private String savedFileName;
	
	public RequestFile(Part part, String uploadDirectoryPath) {
		this.filePart = part;
		this.uploadDir = uploadDirectoryPath;
	}
	
	public boolean save() {
		return save(generateRandomName());
	}
	
	public boolean save(String name) {
		boolean result = false;
		
		File file = new File(this.uploadDir, name);
		
		try (InputStream input = filePart.getInputStream()) {
		    Files.copy(input, file.toPath());
		    setSavedFileName(name);
		    result = true;
		    
		} catch(IOException ioe) {
			setErrorMessage(ioe.getMessage());
		}
		
		return result;
	}
	
	public String getSavedFileName() {
		return this.savedFileName;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public String getExtension() {
		return FilenameUtils.getExtension(filePart.getSubmittedFileName());
	}
	
	/**
	 * 
	 * @return the path from which you can access the file e.g /path/to/file.ext
	 */
	public String getPublicPath() {
		return String.format("/uploads/%s", getSavedFileName());
	}
	
	/**PRIVATE METHODS**/
	
	private void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	private void setSavedFileName(String savedFileName) {
		this.savedFileName = savedFileName;
	}
	
	private String generateRandomName() {
		return Utils.generateRandomFileName(FILE_NAME_LENGTH, getExtension());
	}
}
