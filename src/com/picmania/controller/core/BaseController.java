package com.picmania.controller.core;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.google.gson.Gson;

/**
 * 
 * @author Ibrahima Ndiaye
 *
 */

public abstract class BaseController<Y> {
	private static final String UPLOAD_DIR_PARAM_NAME = "upload.location";
	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private ServletContext servletContext;
	private Map<String, String> placeholderParams = new HashMap<>();
	
	// As the name implies, this method is used to perform a method dependency injection
	public BaseController<Y> inject(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext) {
		this.servletRequest = request;
		this.servletResponse = response;
		this.servletContext = servletContext;
		return this;
	}
	
	// Another dependency injection method
	public BaseController<Y> inject(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext, Map<String, String> paramsMap) {
		inject(request, response, servletContext);
		this.placeholderParams = paramsMap;
		return this;
	}
	
	private Y dao;
	
	// DAO dependency injection method
	public BaseController<Y> inject(Y dao) {
		this.dao = dao;
		return this;
	}
	
	protected Y getDao() {
		return dao;
	}

	// Returns a JSON object corresponding the the serialized responseContent
	protected void sendResponse(Object responseContent) throws IOException {
		servletResponse.setContentType("application/json");
		Gson gson = new Gson();
		
		String JsonRes = gson.toJson(responseContent);
		PrintWriter out = servletResponse.getWriter();
		
		out.print(JsonRes);
		out.flush();
	}
	
	protected void sendErrorResponse(String message) throws IOException {
		Map<String, Object> error = new HashMap<>();
		error.put("message", message);
		
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("success", false);
		response.put("error", error);
		
		sendResponse(response);
	}
	
	protected <T> void sendResponse(String message, T payload) throws IOException {
		BasicResponse<T> basicResponse = new BasicResponse<T>(true, message, payload);
		sendResponse(basicResponse);
	}
	
	protected void sendResponse(String message) throws IOException {
		sendResponse(message, null);
	}
	
	// Returns query string parameters, POST parameters and URL placeholder parameters (See RouteProcessor)
	protected String getParam(String name) {
		if(name == null) return null;
		
		if(name.startsWith(":")) {
			return placeholderParams.get(name.substring(1, name.length()));
			
		} else {
			String value = servletRequest.getParameter(name);
			
			if(value == null || value.trim().isEmpty()) {
				return null;
			}	
			
			return value.trim();
		}
	}
	
	// Checks whether the request has a File
	protected boolean hasRequestFile(String name) throws IOException, ServletException {
		return this.servletRequest.getPart(name) != null;
	}
	
	// Wraps the uploaded file data inside the RequestFile class
	protected RequestFile getRequestFile(String name) throws IOException, ServletException {
		String uploadDir = this.servletContext.getInitParameter(UPLOAD_DIR_PARAM_NAME);
		Part part = this.servletRequest.getPart(name);
		return new RequestFile(part, uploadDir);
	}
	
	public abstract void onRouteTriggered() throws IOException, ServletException;
}
