package com.picmania.controller.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.picmania.controller.core.BaseController;
import com.picmania.controller.core.RouteProcessor;

/**
 * 
 * @author Ibrahima Ndiaye
 *
 */
@MultipartConfig
public abstract class BaseServlet<T> extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private boolean isInitialized = false;
	
	Map<String, BaseController<T>> getMappings = new HashMap<>();
	Map<String, BaseController<T>> postMappings = new HashMap<>();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response, getMappings);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response, postMappings);
	}
	
	// Processes the request with the given Map<String, BaseController<T>> that corresponds
	// to the list of "routes" and their corresponding controller
	private void processRequest(HttpServletRequest request, HttpServletResponse response, Map<String, BaseController<T>> mappings) throws IOException, ServletException {
		String pathInfo = request.getPathInfo(); // Path info = what follows the servlet path
		String requestedUrl = RouteProcessor.normalize(pathInfo); // We add trailing and beginning slashes /
		
		// We loop through the list of entries in order to retrieve the controller
		// in charge of handling this request
		for(Map.Entry<String, BaseController<T>> entry : mappings.entrySet()) {
			String routeDefinition = RouteProcessor.normalize(entry.getKey()); // We retrieve the route definition
			BaseController<T> controller = entry.getValue(); // Controller
			
			// If the route definition has placeholder parameters, we transform it to
			// a regular expression using RouteProcessor helper methods then we check whether that
			// expression matches the requested URL. If it matches, then we extract the parameters
			// from the URL and we save it into a Map
			if(RouteProcessor.hasPlaceholderParam(routeDefinition) 
					&& RouteProcessor.matches(routeDefinition, requestedUrl)) {
				Map<String, String> paramsMap = RouteProcessor.extractParams(routeDefinition, requestedUrl);
				
				// After retrieving the parameters, we simply inject it to the controller
				// alongside it's other dependencies
				controller.inject(request, response, getServletContext(), paramsMap)
					.inject(getDao())
					.onRouteTriggered(); //We trigger the event (handled by controller implementation)
				
				return;
				
			// If the defined route has no placeholder parameters, then we perform a basic
			// String comparison. If it succeeds, we inject the controller's dependencies
			// before triggering the onRouteTriggered event
			} else if(routeDefinition.equals(requestedUrl)) {
				controller.inject(request, response, getServletContext())
					.inject(getDao())
					.onRouteTriggered();
				
				return;
			}
		}
		
		response.getWriter().append("Default response served at: ").append(request.getContextPath());
	}

	// Adds a new URL <-> Controller mapping
	protected void addGETMapping(String uri, BaseController<T> controller) {
		getMappings.put(uri, controller);
	}
	
	// Adds a new URL <-> Controller mapping
	protected void addPOSTMapping(String uri, BaseController<T> controller) {
		postMappings.put(uri, controller);
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(!isInitialized) {
			initUrlMappings();
			isInitialized = false;
		}
		
		// Allow Cross Origin Resource Sharing
		resp.setHeader("Access-Control-Allow-Origin", "*");
		resp.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		resp.setHeader("Access-Control-Allow-Headers", req.getHeader("Access-Control-Request-Headers"));
		
		super.service(req, resp);
	}

	// Initializes routes definition and controllers binding
	protected abstract void initUrlMappings();
	
	// Returns the DAO that will be injected to the controller
	protected abstract T getDao();
}
