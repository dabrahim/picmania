package com.picmania.controller.servlet;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;

import com.picmania.controller.user.CreateUserController;
import com.picmania.controller.user.FindUserController;
import com.picmania.controller.user.GetUserAlbumsController;
import com.picmania.controller.user.GetUsersController;
import com.picmania.controller.user.UserLoginController;
import com.picmania.persistence.dao.UserDAO;


@WebServlet(urlPatterns = {"/api/users/*"})
public class UserServlet extends BaseServlet<UserDAO> {
	private static final long serialVersionUID = 1L;
	
	@EJB
	UserDAO userDao;

	@Override
	protected void initUrlMappings() {
		// GET Mappings
		addGETMapping("/", new GetUsersController());
		addGETMapping("/{i:id}", new FindUserController());
		addGETMapping("/{i:id}/albums", new GetUserAlbumsController());
		
		// POST Mappings
		addPOSTMapping("/", new CreateUserController());
		addPOSTMapping("/login", new UserLoginController());
	}

	@Override
	protected UserDAO getDao() {
		return userDao;
	}
}
