package com.picmania.controller.servlet;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;

import com.picmania.controller.album.CreateAlbumController;
import com.picmania.controller.album.GetAlbumsController;
import com.picmania.persistence.dao.AlbumDAO;

@WebServlet(urlPatterns = {"/api/albums/*"})
public class AlbumServlet extends BaseServlet<AlbumDAO> {
	private static final long serialVersionUID = 1L;

	@EJB
	private AlbumDAO albumDAO;

	@Override
	protected void initUrlMappings() {
		addGETMapping("/", new GetAlbumsController());
		
		// POST Mappings
		addPOSTMapping("/", new CreateAlbumController());
	}
	
	@Override
	protected AlbumDAO getDao() {
		return albumDAO;
	}
}
