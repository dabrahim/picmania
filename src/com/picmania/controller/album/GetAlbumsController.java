package com.picmania.controller.album;

import java.io.IOException;

import com.picmania.controller.core.BaseController;
import com.picmania.persistence.dao.AlbumDAO;

public class GetAlbumsController extends BaseController<AlbumDAO> {

	@Override
	public void onRouteTriggered() throws IOException {
		sendResponse(getDao().findAll());
	}

}
