package com.picmania.controller.album;

import java.io.IOException;

import com.picmania.controller.core.BaseController;
import com.picmania.persistence.dao.AlbumDAO;
import com.picmania.persistence.model.Album;
import com.picmania.persistence.model.User;

public class CreateAlbumController extends BaseController<AlbumDAO> {

	@Override
	public void onRouteTriggered() throws IOException {
		String name = getParam("name");
		String description = getParam("description");
		boolean isPublic = Boolean.getBoolean(getParam("name"));
		
		Long userId = Long.parseLong(getParam("userId"));
		User user = new User();
		user.setId(userId);
		
		Album album = new Album(description, isPublic, name, user);
		
		getDao().create(album);
		
		sendResponse("Album successfully created !", album);
	}

}
