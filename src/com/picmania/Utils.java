package com.picmania;

import java.util.Random;

import org.apache.commons.codec.digest.DigestUtils;

public class Utils {
	
	public static String generateRandomFileName(int length, String extension) {
		return generateRandomChars(length - extension.length() - 1) + '.' + extension;
	}

	private static String generateRandomChars(String candidateChars, int length) {
	    StringBuilder sb = new StringBuilder();
	    Random random = new Random();
	    for (int i = 0; i < length; i++) {
	        sb.append(candidateChars.charAt(random.nextInt(candidateChars
	                .length())));
	    }

	    return sb.toString();
	}
	
	private static String generateRandomChars(int length) {
	    return generateRandomChars("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_", length);
	}
	
	public static String hash(String str) {
		return DigestUtils.sha256Hex(str);
	}
}
