package com.picmania;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

public class TokenManager {
	
	private static final String USER_ID_CLAIM = "userId";
	private static final String TOKEN_PRIVATE_KEY = "$@A0_o-7pqOW0H8@-azpmln1&#";
	private static final String TOKEN_ISSUER = "PicManiaApp";

	public static String getJwtToken(Long userId) {
		String token = null;
		
		try {
			Algorithm algorithm = Algorithm.HMAC256(TOKEN_PRIVATE_KEY);
			token = JWT.create()
					.withIssuer(TOKEN_ISSUER)
					.withIssuedAt(new Date())
					.withClaim(USER_ID_CLAIM, userId)
					.sign(algorithm);
			
		} catch (JWTCreationException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return token;
	}
	
	public static boolean isTokenValid(String token) {
		boolean isValid = false;
		
		try {
		    decodeToken(token);
		    
		} catch (JWTVerificationException | IllegalArgumentException | UnsupportedEncodingException exception){
			// TODO: Log this error
		}
		
		return isValid;
	}
	
	private static DecodedJWT decodeToken(String token) throws IllegalArgumentException, UnsupportedEncodingException, JWTVerificationException {
		DecodedJWT jwt = null;
		
		Algorithm algorithm = Algorithm.HMAC256(TOKEN_PRIVATE_KEY);
	    JWTVerifier verifier = JWT.require(algorithm)
	        .withIssuer(TOKEN_ISSUER)
	        .build();
	    jwt = verifier.verify(token);
		
		return jwt;
	}
	
	public static Long extractUserId(String token) {
		Long userId = 0L;
		
		try {
			DecodedJWT jwt = decodeToken(token);
			Claim userIdClaim = jwt.getClaim(USER_ID_CLAIM);
			
			if(!userIdClaim.isNull()) {
				userId = userIdClaim.asLong();
			}
			
		} catch (IllegalArgumentException | UnsupportedEncodingException | JWTVerificationException e) {
			// TODO: Log this error
			e.printStackTrace();
		}
		
		return userId;
	}
	
}
