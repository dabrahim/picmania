<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>This is the sample page</title>
</head>
<body>
	<h1>This page was served by the controller</h1>
	<h2>The received id is: ${ id }</h2>
</body>
</html>
